# Copyright 2009 Mike Kelly
# Distributed under the terms of the GNU General Public License v2

require perl-module [ module_author=PJCJ ]

SUMMARY="Code coverage metrics for Perl"
DESCRIPTION="
This module provides code coverage metrics for Perl. Code coverage metrics describe how thoroughly
tests exercise code. By using Devel::Cover you can discover areas of code not exercised by your
tests and determine which tests to create to increase coverage. Code coverage can be considered as
an indirect measure of quality.
"

SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-perl/B-Debug
        dev-perl/HTML-Parser[>=3.69]
    recommendation:
        dev-perl/JSON-MaybeXS [[
            description = [ Stores the coverage DB in a JSON format ]
        ]]
    suggestion:
        dev-perl/Browser-Open [[
            description = [ Used to launch a web brower for HTML report formats ]
        ]]
        dev-perl/Perl-Tidy[>=20060719] [[
            description = [ Adds syntax highlighting to some HTML backends ]
        ]]
        dev-perl/Pod-Coverage[>=0.06] [[
            description = [ Tells you how well you have documented your modules ]
        ]]
        dev-perl/Template-Toolkit[>=2.00] [[
            description = [ Required to run some HTML Backends ]
        ]]
    test:
        dev-perl/Test-Differences
"

